# 2x12V/500mA POWER-SUPPLY

The purpose of this page is to explain step by step the realization of an ultra low noise 2x12V power supply.

It has been designed to power a preamplifier or an active crossover.

The power supply uses the following components :

 * a 2x12V 10VA transformer
 * a rectifier bridge
 * 4 4700µF 25V capacitors
 * a 100nF and a 100pF capacitors
 * an LT3045/LT3094 regulator module

### ELECTRONICS

The schema is made using KICAD.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2021/09/alimentation-2x12v-faible-bruit.html

